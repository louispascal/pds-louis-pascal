package backendserver;

import connectionPool.DataSource;

import java.sql.*;

public class WindowRequest {
    private final static DataSource ds = DataSource.getInstance();

    public static String selectWindow(String Building , String Stage, String Room, String Face) throws Exception {
        Connection c = ds.getConnection();
        Statement stmt = c.createStatement();
        StringBuilder result = new StringBuilder();
        System.out.println("SELECT * FROM Windows Where Building = " + Building + " Stage = " + Stage + " AND Room = " + Room + " Face=" + Face);
        ResultSet rs = stmt.executeQuery("SELECT * FROM Windows Where Stage = " + Stage + " AND Room = " + Room + " Face=" + Face);
        while (rs.next()) {
            String Shade = rs.getString("Shade");
            String Curtains = rs.getString("Curtains");
            result.append("Shade : " + Shade + "| Curtains : " + Curtains);
        }
        ds.putBackConnection(c);
        return result.toString();
    }

    public static String updateWindow(String Building, String Stage, String Room, String Face, String Shade, String Curtains, String New_Shade, String New_Curtains) throws Exception {
        Connection c = ds.getConnection();
        Statement stmt = c.createStatement();
        String update = "UPDATE Windows SET shade = '" + Integer.parseInt(Shade) + "', Curtains = '" + Integer.parseInt(Curtains) + "' WHERE Building = '" + Building + "' Stage ='" + Integer.parseInt(Stage) + "'AND Room = '" + Integer.parseInt(Room) + "' AND Face = '" + Face;
        System.out.println(update);
        stmt.executeUpdate(update);
        ds.putBackConnection(c);
        return "lines updated";
    }


    public static String Auto(String luminosity, String hour) throws Exception {
        Connection c = ds.getConnection();
        Statement stmt = c.createStatement();
        StringBuilder result = new StringBuilder();
        System.out.println("SELECT * FROM sensor");
        ResultSet rs = stmt.executeQuery("SELECT * FROM sensor");
        while(rs.next()){
            luminosity = rs.getString("Luminosity");
            hour = rs.getString("hour");
            result.append("luminosity : " + luminosity + "| hour : " + hour);
        }
        ds.putBackConnection(c);
        return result.toString();
    }
}
