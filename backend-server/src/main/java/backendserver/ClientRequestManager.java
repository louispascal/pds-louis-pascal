package backendserver;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import connectionPool.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.SocketHandler;

import static java.lang.Thread.sleep;

public class ClientRequestManager implements Runnable {
    private final static DataSource ds = DataSource.getInstance();
    private static final Logger logger = LoggerFactory.getLogger(ClientRequestManager.class.getName());
    private final InputStream in;
    private final OutputStream out;
    private final static String name = "client-thread-manager";
    private Thread self;
    private Connection c;

    public ClientRequestManager(Socket socket) throws IOException {
        in = socket.getInputStream();
        out = socket.getOutputStream();
        self = new Thread(this, name);
        self.run();
    }

    public void join() throws InterruptedException {
        self.join();
    }

    @Override
    public void run() {
        byte inputData[];
        try{
            while (in.available() == 0){}
            inputData = new byte[in.available()];
            in.read(inputData);
            logger.info("data received {}", new String(inputData));
            final ObjectMapper mapper = new ObjectMapper();
            DataOutputStream dos = new DataOutputStream(out);
            c = ds.getConnection();
            requestManager(mapper, dos, c, inputData);
            ds.putBackConnection(c);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void requestManager(ObjectMapper mapper, DataOutputStream dos, Connection c, byte[] inputData) throws Exception {
        HashMap<String, Object> requestMap = mapper.readValue(inputData, HashMap.class);
        logger.info("request : " + requestMap.get("request"));
        if (requestMap.get("request").equals("read")){
            try {
                byte[] response = Crud.select();
                dos.write(response);
                logger.info("read request data sent!");
            }catch(NullPointerException e) {
                byte[] response = String.format("The connection pool is saturated, try again").
                        getBytes(StandardCharsets.UTF_8);
                logger.info("The connection pool is saturated, try again");
                dos.write(response);
            }
        }

        if(requestMap.get("request").equals("insert")){
            String response = Crud.insert((String)requestMap.get("last_name"), (String)requestMap.get("first_name"), (int) requestMap.get("age"));
            dos.write(response.getBytes(StandardCharsets.UTF_8));
            logger.info("insert request done");
        }

        if(requestMap.get("request").equals("delete")){
            String response = Crud.delete((String)requestMap.get("last_name"), (String)requestMap.get("first_name"));
            dos.write(response.getBytes(StandardCharsets.UTF_8));
            logger.info("delete request done");
        }

        if(requestMap.get("request").equals("update")){
            String response = Crud.update((String)requestMap.get("first_name"), (String)requestMap.get("last_name"), (String) requestMap.get("new_first_name"), (String) requestMap.get("new_last_name"), (int) requestMap.get("new_age"));
            dos.write(response.getBytes(StandardCharsets.UTF_8));
            logger.info("update request done");
        }


        if(requestMap.get("request").equals("buildings")){
            PreparedStatement preparedStatement = c.prepareStatement(
                    "SELECT building_name FROM building");
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while(rs.next()){
                HashMap<String, Object> row = new HashMap<>();
                row.put("building_name", rs.getString("building_name"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));

            logger.info("data sent: " + rowList.toString());
        }


        if(requestMap.get("request").equals("Floor")){
            String building_name = (String) requestMap.get("building_name");
            PreparedStatement preparedStatement = c.prepareStatement(
                    "SELECT floor_number,id_floor FROM floors WHERE building_name = ?");
            preparedStatement.setString(1, building_name);
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while(rs.next()){
                HashMap<String, Object> row = new HashMap<>();
                row.put("floor_number", rs.getString("floor_number"));
                row.put("floor_id", rs.getString("id_floor"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));

            logger.info("data sent: " + rowList.toString());
        }


        if(requestMap.get("request").equals("Room")){
            int floor_id = (int) requestMap.get("floors_id");
            PreparedStatement preparedStatement = c.prepareStatement(
                    "SELECT room_number,id_room FROM room where id_floor = ? ");
            preparedStatement.setInt(1, floor_id);
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while(rs.next()){
                HashMap<String, Object> row = new HashMap<>();
                row.put("room_number", rs.getString("room_number"));
                row.put("room_id", rs.getString("id_room"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));

            logger.info("data sent: " + rowList.toString());
        }


        if(requestMap.get("request").equals("WindowsO")){
            int room_id = (int) requestMap.get("room_id");
            PreparedStatement preparedStatement = c.prepareStatement(
                    "SELECT windows_orientation,id_windows FROM windows where id_room = ?");
            preparedStatement.setInt(1,room_id);
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while(rs.next()){
                HashMap<String, Object> row = new HashMap<>();
                row.put("windows_orientation", rs.getString("windows_orientation"));
                row.put("id_windows", rs.getString("id_windows"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));

            logger.info("data sent: " + rowList.toString());
        }


        if(requestMap.get("request").equals("WindowsData")){
            int windows_id = (int) requestMap.get("windows_id");
            PreparedStatement preparedStatement = c.prepareStatement(
                    "SELECT shade_value,curtains_value FROM windows where id_windows=?");
            preparedStatement.setInt(1, windows_id);
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while(rs.next()){
                HashMap<String, Object> row = new HashMap<>();
                row.put("shade", rs.getInt("shade_value"));
                row.put("curtains", rs.getInt("curtains_value"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));

            logger.info("data sent: " + rowList.toString());
        }

        if(requestMap.get("request").equals("updateWindowsStatus")){
                int windows_id = (int) requestMap.get("id_windows");
                int shade_value = (int) requestMap.get("shade");
                int curtain_value = (int) requestMap.get("curtain");
            PreparedStatement preparedStatement = c.prepareStatement(
                    "UPDATE windows SET shade_value=? ,curtains_value= ? WHERE id_windows = ?");
            preparedStatement.setInt(3 , windows_id);
            preparedStatement.setInt(1,shade_value);
            preparedStatement.setInt(2,curtain_value);
            preparedStatement.executeUpdate();
            logger.info("data modified you've set shade_value to "+ shade_value + " and curtains_value to " + curtain_value);
        }

        if(requestMap.get("request").equals("SensorsValues")){
            String building_name = (String) requestMap.get("building_name");
            PreparedStatement preparedStatement = c.prepareStatement(
                    "SELECT luminosity,temperature FROM sensors WHERE building_name = ?");
            preparedStatement.setString(1, building_name);
            ResultSet rs = preparedStatement.executeQuery();
            ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
            while(rs.next()){
                HashMap<String, Object> row = new HashMap<>();
                row.put("luminosity", rs.getInt("luminosity"));
                row.put("temperature", rs.getInt("temperature"));
                rowList.add(row);
            }
            dos.write(mapper.writeValueAsBytes(rowList));

            logger.info("data sent: " + rowList.toString());
        }
    }
}