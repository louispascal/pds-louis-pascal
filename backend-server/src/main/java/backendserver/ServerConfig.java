package backendserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class ServerConfig {
    private static final Logger logger = LoggerFactory.getLogger(ServerConfig.class.getName());
    private final static String episenServerConfEnvVar = "EPISEN_SRV_CONFIG";
    private final String episenServerConfigFileLocation;

    private ServerCoreConfig config;

    public ServerConfig() throws IOException {
        episenServerConfigFileLocation = System.getenv(episenServerConfEnvVar);
        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        config = mapper.readValue(new File(episenServerConfigFileLocation), ServerCoreConfig.class);
    }

    public ServerCoreConfig getConfig() {
        return config;
    }
}