package backendserver;

import java.util.Map;

public class Json {
    private String request;
    private Map<String, String> data;

    public Json() {
    }

    public Json(String request, Map<String, String> data) {
        this.request = request;
        this.data = data;
    }

    public String getRequest() {
        return request;
    }

    public void setRequest(String request) {
        this.request = request;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Json{" +
                "request='" + request + '\'' +
                ", data=" + data +
                '}';
    }
}
