package backendserver;

public class OutOfConnectionException extends Exception{
    public OutOfConnectionException(String msg){
        super(msg);
    }
}
