package backendserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import connectionPool.DataSource;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

public class Crud {
    private final static DataSource ds = DataSource.getInstance();

    public static String delete(String last_name, String first_name) throws Exception {
        int n = 0;
        Connection c = ds.getConnection();
        Statement statement = c.createStatement();
        System.out.println("last name = " + last_name + " first_name =" + first_name);
        n += statement.executeUpdate("DELETE FROM Client WHERE last_name = '" + last_name + "' and first_name = '" + first_name + "'");
        ds.putBackConnection(c);
        return n + " lines deleted";
    }

    public static byte[] select() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        ArrayList<HashMap<String, Object>> rowList = new ArrayList<>();
        Connection c = ds.getConnection();
        Statement stmt = c.createStatement();
        StringBuilder result =  new StringBuilder();
        ResultSet rs = stmt.executeQuery("SELECT * FROM users");
        while (rs.next()) {
            HashMap<String, Object> row = new HashMap<>();
            row.put("id_user", rs.getInt("id_user"));
            row.put("first_name", rs.getString("first_name"));
            row.put("last_name", rs.getString("last_name"));
            row.put("age", rs.getInt("age"));
            rowList.add(row);
        }
        ds.putBackConnection(c);
        return mapper.writeValueAsBytes(rowList);
    }

    public static String insert(String last_name, String first_name, int age) throws Exception {
        System.out.println("insert");
        Connection c = ds.getConnection();
        Statement stmt = c.createStatement();
        stmt.executeUpdate("INSERT INTO Client(last_name,first_name, age) VALUES ('" + last_name + "', '" + first_name + "', '" + age + "')");
        ds.putBackConnection(c);
        return "lines added";
    }

    public static String update(String first_name, String last_name, String new_first_name, String new_last_name, int new_age) throws Exception {
        Connection c = ds.getConnection();
        int n;
        PreparedStatement pstmt = c.prepareStatement(
                "UPDATE users SET first_name = ?, last_name = ?, age = ? " +
                        "WHERE first_name = ? AND last_name = ?"
        );
        pstmt.setString(1, new_first_name);
        pstmt.setString(2, new_last_name);
        pstmt.setInt(3, new_age);
        pstmt.setString(4, first_name);
        pstmt.setString(5, last_name);
        n = pstmt.executeUpdate();
        ds.putBackConnection(c);
        return n + "lines updated";
    }
}
