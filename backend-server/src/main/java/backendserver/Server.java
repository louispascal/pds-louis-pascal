package backendserver;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;

public class Server {
    private ServerSocket serverSocket;
    private final static Logger logger = LoggerFactory.getLogger(Server.class.getName());

    public Server(final ServerConfig config) throws IOException {
        serverSocket = new ServerSocket(config.getConfig().getListenPort());
    }

    public void serve() throws IOException {
        try {
            while (true) {
                logger.info("waiting for client..");
                final Socket socket = serverSocket.accept();
                logger.debug("got a requester");
                logger.info("Client IP : " + socket.getInetAddress().toString());
                final ClientRequestManager manager = new ClientRequestManager(socket);
                try {
                    manager.join();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        } catch (SocketTimeoutException e) {
            logger.info("Got a timeout ");
        }
    }

}
