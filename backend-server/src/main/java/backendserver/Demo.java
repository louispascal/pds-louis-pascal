package backendserver;

import java.io.IOException;

public class Demo {

    public static ServerConfig config;

    public static void main(String[] args) throws IOException, InterruptedException {
        config = new ServerConfig();
        Server server = new Server(config);
        server.serve();
    }
}
