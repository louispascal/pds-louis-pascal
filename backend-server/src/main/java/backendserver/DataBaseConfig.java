package backendserver;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;

public class DataBaseConfig {
    private static final Logger logger = LoggerFactory.getLogger(DataBaseConfig.class.getName());
    private final static String dataBaseConfigEnvVar = "DATA_BASE_CONFIG";
    private  String dataBaseConfigFileLocation;

    private DataBaseCoreConfig config;

    public DataBaseConfig() throws IOException {
        dataBaseConfigFileLocation = System.getenv(dataBaseConfigEnvVar);
        final ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        config = mapper.readValue(new File(dataBaseConfigFileLocation), DataBaseCoreConfig.class);
    }

    public DataBaseCoreConfig getConfig() {
        return config;
    }


}
