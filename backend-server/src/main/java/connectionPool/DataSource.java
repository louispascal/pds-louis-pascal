package connectionPool;

import javax.xml.crypto.Data;
import java.sql.Connection;

public class DataSource {
    private static final JDBCConnectionPool pool = new JDBCConnectionPool();

    private DataSource() {
        pool.addConnection();
    }
    private static DataSource ds = new DataSource();

    public static DataSource getInstance() {
        return ds;
    }
    public static JDBCConnectionPool getPool() {
        return pool;
    }

    public static Connection getConnection() throws Exception {
        Connection c;
        synchronized (pool) {
            c = pool.getConnection();
        }
        return c;
    }

    public static void putBackConnection(Connection c) {
        synchronized (pool) {
            pool.putBackConnection(c);
        }
    }

    public static void closeConnections() {
        synchronized (pool) {
            pool.closeConnections();
        }
    }

}
