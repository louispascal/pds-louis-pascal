package connectionPool;

import backendserver.DataBaseConfig;
import backendserver.DataBaseCoreConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class JDBCConnectionPool {
    private final static Logger logger = LoggerFactory.getLogger(JDBCConnectionPool.class.getName());
    private String driver;
    private String db_url;
    private String db_user;
    private String db_pwd;
    private int min_connection;
    private int max_connection;
    private DataBaseConfig dataBaseConfig;

    //The connections
    private final List<Connection> connections;

    private static int connections_number;

    public JDBCConnectionPool(){
        logger.info("ConnectionPool running");
        connections = new ArrayList<>();
        try {
            dataBaseConfig = new DataBaseConfig();
        } catch (IOException e) {
            e.printStackTrace();
        }
        driver = dataBaseConfig.getConfig().getDriver();
        db_url = dataBaseConfig.getConfig().getDb_url();
        db_user = dataBaseConfig.getConfig().getDb_user();
        db_pwd = dataBaseConfig.getConfig().getDb_pwd();
        min_connection = dataBaseConfig.getConfig().getMin_connection();
        max_connection = dataBaseConfig.getConfig().getMax_connection();
    }

    //Loading the driver
    public void loadDriver() {
        try {
            Class.forName(driver);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void addConnection() {
        int i = min_connection;

        loadDriver();
        try {
            if (max_connection - connections_number > 0) {
                while (i > 0) {
                    Connection c = DriverManager.getConnection(db_url, db_user, db_pwd);
                    connections.add(c);
                    connections_number++;
                    i--;
                }
                logger.info("{} Connections successfully added to the connection pool, and {} are available to use",
                        min_connection, connections.size());
            } else
                logger.info("Can't add more connections, Connection limit is: {}", max_connection);
        } catch (SQLException e) {
            logger.info(e.getMessage());
        }
    }

    public void additionalConnection() {
        loadDriver();
        try {
            if (max_connection - connections.size() > 0) {
                Connection c = DriverManager.getConnection(db_url, db_user, db_pwd);
                connections.add(c);
                logger.info("1 Connection successfully added to the connection pool, and {} are available to use",
                        connections.size());
            } else
                logger.info("Can't add more connections, Connection limit is: {}", max_connection);
        } catch (SQLException e) {
            logger.info(e.getMessage());
        }
    }

    public Connection getConnection() {
        Connection c;
        if (!connections.isEmpty()){
            c = connections.get(0);
            connections.remove(0);
        }
        else{
            additionalConnection();
            c = connections.get(0);
        }
        return c;
    }

    public void putBackConnection(Connection c) {
        connections.add(c);
    }

    public void closeConnections() {
        boolean flag = true;
        for (Connection c : connections) {
            try {
                c.close();
            } catch (SQLException e) {
                flag = false;
                logger.info("Connection {} could not be closed with error msg : {}!", c, e);
            }
        }
        if (flag)
            logger.info("All connections closed successfully!");
        else
            logger.info("Some connections didn't close correctly");
    }

    public List<Connection> getConnections() {
        return connections;
    }
}
