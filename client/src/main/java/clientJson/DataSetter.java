package clientJson;


import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class DataSetter {
    private WindowsRequest windowsRequest;
    private ArrayList<HashMap<String, Object>> answer;
    private HashMap<String, Object> request;

    public void updateWindowsStatus(int id, int curtain, int shade) throws IOException {
        windowsRequest = new WindowsRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "updateWindowsStatus");
        request.put("id_windows", id);
        request.put("curtain", curtain);
        request.put("shade", shade);
        windowsRequest.startUpdate(request);
    }
}
