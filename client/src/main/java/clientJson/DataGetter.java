package clientJson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class DataGetter {
    private WindowsRequest windowsRequest;
    private ArrayList<HashMap<String, Object>> answer;
    private HashMap<String, Object> request;


    public ArrayList<HashMap<String, Object>> Buildings() throws IOException {
        windowsRequest = new WindowsRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "buildings");
        answer = windowsRequest.start(request);
        return(answer);
    }

    public ArrayList<HashMap<String, Object>> Floor(String building_name) throws IOException {
        windowsRequest = new WindowsRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "Floor");
        request.put("building_name", building_name);
        answer = windowsRequest.start(request);
        return(answer);
    }

    public ArrayList<HashMap<String, Object>> Room(int floor_id) throws IOException{
        windowsRequest = new WindowsRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "Room");
        request.put("floors_id", floor_id);
        answer = windowsRequest.start(request);
        return(answer);
    }

    public ArrayList<HashMap<String, Object>> WindowsO(int room_id) throws IOException {
        windowsRequest =  new WindowsRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "WindowsO");
        request.put("room_id", room_id);
        answer = windowsRequest.start(request);
        return(answer);
    }

    public ArrayList<HashMap<String, Object>> WindowsData( int Windows_id) throws IOException {
        windowsRequest =  new WindowsRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request","WindowsData");
        request.put("windows_id", Windows_id);

        answer = windowsRequest.start(request);
        return(answer);
    }

    public ArrayList<HashMap<String, Object>> SensorsValues(String building_name) throws IOException {
        windowsRequest = new WindowsRequest(new ClientConf());
        request = new HashMap<>();
        request.put("request", "SensorsValues");
        request.put("building_name", building_name);
        answer = windowsRequest.start(request);
        return (answer);
    }
}

