package clientJson.window;

import clientJson.DataGetter;
import clientJson.DataSetter;
import com.intellij.uiDesigner.core.GridConstraints;
import com.intellij.uiDesigner.core.GridLayoutManager;
import com.intellij.uiDesigner.core.Spacer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class Windows_Value {
    private DataGetter dataGetter;
    private DataSetter dataSetter;
    private JPanel panel1;
    private JTextField curtainsTextField;
    private JTextField shadeTextField;
    private JButton automatiqueButton;
    private JButton validerButton;
    private JPanel paneLeft;
    private JPanel paneRight;
    private JPanel paneMiddle;
    ArrayList<HashMap<String, Object>> WindowData;
    ArrayList<HashMap<String, Object>> SensorData;


    public Windows_Value(int selectedWindow, String selectedBuilding) throws IOException {
        JFrame frame1 = new JFrame();
        dataGetter = new DataGetter();
        dataSetter = new DataSetter();
        frame1.setDefaultCloseOperation(frame1.EXIT_ON_CLOSE);
        System.out.println(selectedBuilding);
        System.out.println(selectedWindow);
        frame1.setSize(500, 200);
        frame1.setTitle("Choissisez vos parametres");
        frame1.getContentPane().add(panel1);
        frame1.setVisible(true);
        WindowData = dataGetter.WindowsData(selectedWindow);
        for (HashMap<String, Object> windowData : WindowData) {
            curtainsTextField.setText((String) String.valueOf(windowData.get("curtains")));
            System.out.println(windowData.get("curtains"));
            shadeTextField.setText((String) String.valueOf(windowData.get("shade")));
            System.out.println(windowData.get("shade"));
        }
        SensorData = dataGetter.SensorsValues(selectedBuilding);
        for (HashMap<String, Object> sensorData : SensorData) {
            Integer luminosity = (int) sensorData.get("luminosity");
            Integer temperature = (int) sensorData.get("temperature");
        }
        automatiqueButton.addActionListener(new ActionListener() {
                                                @Override
                                                public void actionPerformed(ActionEvent e) {
                                                    int curtain_value = Integer.parseInt(curtainsTextField.getText());
                                                    int shade_value = Integer.parseInt(shadeTextField.getText());
                                                    try {
                                                        SensorData = dataGetter.SensorsValues(selectedBuilding);
                                                    } catch (IOException ioException) {
                                                        ioException.printStackTrace();
                                                    }
                                                    for (HashMap<String, Object> sensorData : SensorData) {
                                                        Integer luminosity = (int) sensorData.get("luminosity");
                                                        Integer temperature = (int) sensorData.get("temperature");
                                                        if (luminosity > 5000) {
                                                            curtain_value = 100;
                                                            shade_value = 101;
                                                        } else {
                                                            if (luminosity < 2000) {
                                                                curtain_value = 0;
                                                                shade_value = 0;
                                                            } else {
                                                                curtain_value = temperature * 4;
                                                                shade_value = temperature * 4;

                                                            }
                                                        }

                                                        curtainsTextField.setText(String.valueOf(curtain_value));
                                                        shadeTextField.setText(String.valueOf(shade_value));
                                                    }
                                                }

                                            }

        );
        validerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int curtains_value = Integer.parseInt(curtainsTextField.getText());
                int shade_value = Integer.parseInt(shadeTextField.getText());
                if (curtains_value < 0) {
                    curtainsTextField.setText("0");
                    curtains_value = 0;
                }
                if (curtains_value > 100) {
                    curtainsTextField.setText("100");
                    curtains_value = 100;
                }
                if (shade_value < 0) {
                    shadeTextField.setText("0");
                    shade_value = 0;
                }
                if (shade_value > 100) {
                    shadeTextField.setText("100");
                    shade_value = 100;
                }
                try {
                    dataSetter.updateWindowsStatus(selectedWindow, curtains_value, shade_value);
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                try {
                    WindowSelector windowSelector;
                    windowSelector = new WindowSelector();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
                frame1.dispose();
            }
        });
    }

    {
// GUI initializer generated by IntelliJ IDEA GUI Designer
// >>> IMPORTANT!! <<<
// DO NOT EDIT OR ADD ANY CODE HERE!
        $$$setupUI$$$();
    }

    /**
     * Method generated by IntelliJ IDEA GUI Designer
     * >>> IMPORTANT!! <<<
     * DO NOT edit this method OR call it in your code!
     *
     * @noinspection ALL
     */
    private void $$$setupUI$$$() {
        panel1 = new JPanel();
        panel1.setLayout(new GridLayoutManager(1, 3, new Insets(0, 0, 0, 0), -1, -1));
        paneLeft = new JPanel();
        paneLeft.setLayout(new GridLayoutManager(6, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel1.add(paneLeft, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        shadeTextField = new JTextField();
        shadeTextField.setText("Shade");
        paneLeft.add(shadeTextField, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final Spacer spacer1 = new Spacer();
        paneLeft.add(spacer1, new GridConstraints(5, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final JPanel panel2 = new JPanel();
        panel2.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        paneLeft.add(panel2, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel3 = new JPanel();
        panel3.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        paneLeft.add(panel3, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label1 = new JLabel();
        label1.setText("Teinte");
        paneLeft.add(label1, new GridConstraints(1, 1, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel4 = new JPanel();
        panel4.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        paneLeft.add(panel4, new GridConstraints(3, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        automatiqueButton = new JButton();
        automatiqueButton.setText("Automatique");
        paneLeft.add(automatiqueButton, new GridConstraints(4, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        paneRight = new JPanel();
        paneRight.setLayout(new GridLayoutManager(6, 2, new Insets(0, 0, 0, 0), -1, -1));
        panel1.add(paneRight, new GridConstraints(0, 2, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        curtainsTextField = new JTextField();
        curtainsTextField.setText("curtains");
        paneRight.add(curtainsTextField, new GridConstraints(2, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_WANT_GROW, GridConstraints.SIZEPOLICY_FIXED, null, new Dimension(150, -1), null, 0, false));
        final Spacer spacer2 = new Spacer();
        paneRight.add(spacer2, new GridConstraints(5, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_VERTICAL, 1, GridConstraints.SIZEPOLICY_WANT_GROW, null, null, null, 0, false));
        final JPanel panel5 = new JPanel();
        panel5.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        paneRight.add(panel5, new GridConstraints(2, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JPanel panel6 = new JPanel();
        panel6.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        paneRight.add(panel6, new GridConstraints(0, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        final JLabel label2 = new JLabel();
        label2.setText("Volet");
        paneRight.add(label2, new GridConstraints(1, 0, 1, 1, GridConstraints.ANCHOR_WEST, GridConstraints.FILL_NONE, GridConstraints.SIZEPOLICY_FIXED, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        final JPanel panel7 = new JPanel();
        panel7.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        paneRight.add(panel7, new GridConstraints(3, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
        validerButton = new JButton();
        validerButton.setText("Valider");
        paneRight.add(validerButton, new GridConstraints(4, 0, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_HORIZONTAL, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_FIXED, null, null, null, 0, false));
        paneMiddle = new JPanel();
        paneMiddle.setLayout(new GridLayoutManager(1, 1, new Insets(0, 0, 0, 0), -1, -1));
        panel1.add(paneMiddle, new GridConstraints(0, 1, 1, 1, GridConstraints.ANCHOR_CENTER, GridConstraints.FILL_BOTH, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, GridConstraints.SIZEPOLICY_CAN_SHRINK | GridConstraints.SIZEPOLICY_CAN_GROW, null, null, null, 0, false));
    }

    /**
     * @noinspection ALL
     */
    public JComponent $$$getRootComponent$$$() {
        return panel1;
    }
}