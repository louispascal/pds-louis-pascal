package clientJson;

public class ClientCoreConf {
    private String ipAddress;
    private int listenPort;

    public ClientCoreConf() {}

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public int getListenPort() {
        return listenPort;
    }

    public void setListenPort(int listenPort) {
        this.listenPort = listenPort;
    }

    @Override
    public String toString() {
        return "ClientCoreConf{" +
                "ipAddress='" + ipAddress + '\'' +
                ", listenPort=" + listenPort +
                '}';
    }
}
