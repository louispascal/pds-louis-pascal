package clientJson;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

import static java.lang.Thread.sleep;



public class ClientRequest {
    private final static Logger logger = LoggerFactory.getLogger(ClientRequest.class.getName());
    private static String clientRequestFileLocation;
    private static final String requestFileLocationEnvVar = "JSON_CONF";
    private Socket socket;
    private OutputStream outputStream;
    private InputStream inputStream;
    private String jsonOption;

    public ClientRequest(final ClientConf clientConf, String jsonOption) {
        this.jsonOption = jsonOption;
        try {
            socket = new Socket(clientConf.getConf().getIpAddress(), clientConf.getConf().getListenPort());
        } catch (IOException e) {
            e.printStackTrace();
        }
        clientRequestFileLocation = System.getenv(requestFileLocationEnvVar);
    }
    public void start() throws IOException {
        logger.info("Client requested {} option", jsonOption);

        final ObjectMapper mapper = new ObjectMapper();
        byte[] inputData;

        inputStream = socket.getInputStream();
        outputStream = socket.getOutputStream();
        clientRequestFileLocation = clientRequestFileLocation + "\\" + jsonOption + ".json";

        HashMap<String, Object> jsonMap = mapper.readValue(new File(clientRequestFileLocation), HashMap.class);
        jsonMap.put("request", jsonOption);
        outputStream.write(mapper.writeValueAsBytes(jsonMap));
        logger.info("Request submitted");

        while(inputStream.available() == 0) {}
        inputData = new byte[inputStream.available()];
        inputStream.read(inputData);
        String serverAnswer = new String(inputData);
        logger.info("server answer: \n {}", serverAnswer);
    }
}

